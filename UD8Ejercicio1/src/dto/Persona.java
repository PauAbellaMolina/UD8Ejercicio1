package dto;

public class Persona {

	//Atributos
	private String nombre;
	private int edad;
	private String dni;
	private char sexo;
	private double peso;
	private double altura;
	
	final char SEXO = 'H';
	
	public String toString() {
		return "Persona [nombre=" + nombre + ", edad=" + edad + ", dni=" + dni + ", sexo=" + sexo + ", peso=" + peso
				+ ", altura=" + altura + "]";
	}

	//Constructor por defecto
	public Persona() {
		this.nombre="";
		this.edad=0;
		this.dni="00000000A";
		this.sexo=SEXO;
		this.peso=0;
		this.altura=0;
	}
	
	//Constructor con el nombre, edad y sexo, el resto por defecto
	public Persona(String nombre, int edad, char sexo) {
		this.nombre=nombre;
		this.edad=edad;
		this.dni="00000000A";
		this.sexo=SEXO;
		this.peso=0;
		this.altura=0;
	}
	
	//Constructor con todos los atributos como parametro
	public Persona(String nombre, int edad, String dni, char sexo, double peso, double altura) {
		this.nombre=nombre;
		this.edad=edad;
		this.dni=dni;
		this.sexo=SEXO;
		this.peso=peso;
		this.altura=altura;
	}
}
