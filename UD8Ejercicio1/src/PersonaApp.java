import dto.Persona;

public class PersonaApp {

	public static void main(String[] args) {
		Persona p1 = new Persona();
		Persona p2 = new Persona("Pau", 18, 'H');
		Persona p3 = new Persona("Pau", 18, "39470763A", 'M', 65.5, 175);
		
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
	}

}
